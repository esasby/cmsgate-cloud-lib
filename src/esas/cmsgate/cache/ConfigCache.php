<?php


namespace esas\cmsgate\cache;


class ConfigCache
{
    private $uuid;
    private $login;
    private $password;
    private $secret;
    private $configArray;

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param mixed $uuid
     */
    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param mixed $login
     */
    public function setLogin($login): void
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }



    /**
     * @return mixed
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     */
    public function setSecret($secret): void
    {
        $this->secret = $secret;
    }

    /**
     * @return mixed
     */
    public function getConfigArray()
    {
        return $this->configArray;
    }

    public function addToConfigArray($key, $value) {
        $this->configArray[$key] = $value;
        return $this;
    }

    /**
     * @param mixed $configArray
     */
    public function setConfigArray($configArray): void
    {
        $this->configArray = $configArray;
    }


}